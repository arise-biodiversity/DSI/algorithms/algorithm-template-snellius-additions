# Algorithm Template - Snellius Additions
This repository contains the files to run the ARISE detection algorithm in an
Apptainer/Singularity container on the Snellius supercomputer. The ARISE detection
algorithm is a small example AI algorithm template. See this repository for more
information:

[gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template)


## Getting started
The job script (`arise-detection.script`) is used to run the container on Snellius as a
batch job:

- `sbatch arise-detection.script`


To run the commands from the job script interactively (for testing purposes), use:

- `srun --time=00:10:00 --partition=gpu --nodes=1 --gpus=1 --ntasks=1 --cpus-per-task=18 --pty /bin/bash`


<br>
The job script calls the Python script `run-algorithm.py`, which in turn runs the ARISE
detection algorithm. The files from the input directory are then sent to the algorithm
via the analyse endpoint and the result json files are stored in the output directory.
The input directory already contains an image for testing.


## Creating the Singularity image file
The ARISE detection algorithm Docker container has to be converted to a Singularity
image file (.sif). Because of the large size (150 MB), the .sif file is not stored in
this repository. Instead, the steps are described to convert the ARISE detection Docker
container into an Apptainer/Singularity container (which is stored in a .sif file):

- `# Start a local registry (on port 5022):`
- `sudo docker run -d -p 5022:5000 --name local-registry registry:2`

- `# Push the Docker container to the local registry so that Apptainer/Singularity can pull it:`
- `sudo docker ps | grep arise`
- `sudo docker container commit <feb07f68020b> arise-detection`
- `sudo docker image tag arise-detection localhost:5022/arise-detection`
- `sudo docker image push localhost:5022/arise-detection`

- `# You can check the registry contents in a browser: http://localhost:5022/v2/_catalog`
- `# {"repositories":["arise-detection"]}`

- `# Build the Singularity image file from the local Docker registry:`
- `apptainer build --no-https arise-detection.sif docker://localhost:5022/arise-detection:latest`
- `# The --no-https parameter is required as the local registry is not secured.`


## Binding the /usr/src/app directory
In addition to the job script, this repository has a directory called "shared" that is
bound to the /usr/src/app directory (by the --bind parameter). This is done because we 
want to simulate an algorithm that needs to be able to write to a directory that is not
allowed by Singularity, unless the bind option is used. Because the ARISE detection
algorithm expects the arise_server.py file in the /src directory, this file has to be 
copied there from the Docker container. To copy files from the Docker container
directory /usr/src/app to the shared directory, you can follow these steps:

- `sudo docker exec <feb07f68020b> bash -c "mkdir -p /extract; cp -r /usr/src/app/* /extract"`
- `sudo docker cp <feb07f68020b>:/extract/. .`


## Copy the files to Snellius
The required files can be copied from the snellius directory to Snellius with `scp`:

- `scp arise-detection.script <bruijnfd>@snellius.surf.nl:arise-detection/snellius`
- `scp run-algorithm.py <bruijnfd>@snellius.surf.nl:arise-detection/snellius`
- `scp arise-detection.sif <bruijnfd>@snellius.surf.nl:arise-detection/snellius`
- `scp shared/arise_server.py <bruijnfd>@snellius.surf.nl:arise-detection/snellius/shared`


## License
Apache License 2.0.
