import fnmatch
import json
import logging
import os
import psutil
import requests
import shutil
import signal
import subprocess
import time

from subprocess import CalledProcessError

# Python script that runs the example AI algorithm in a Singularity container.
# For the example AI algorithm, see this repository:
# https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template
# ARISE DSI team.
# Version: 0.0.8 (November 4th, 2022).

# Note: this script, the .sif file and the input, output & shared directories are in the
# "snellius" subdirectory. Run the script from the shared directory:
# python3 ../run-algorithm.py

algorithm_name = "arise-detection"

# todo: Remove these lines before committing the file (and restore them afterwards).
# Some parameters that differ between Snellius and a local laptop.
# on_snellius = True
# apptainer_command = "singularity" if on_snellius else "apptainer"
# apptainer_command_path = shutil.which(apptainer_command)
# base_directory_snellius = f"/scratch-shared/{os.environ['USER']}/{algorithm_name}/"
# base_directory_local = "/home/freek/projects/algorithm-template/"
# base_directory = base_directory_snellius if on_snellius else base_directory_local

apptainer_command_path = shutil.which("singularity")
base_directory = f"/scratch-shared/{os.environ['USER']}/{algorithm_name}/"


def main():
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(message)s",
        level=logging.DEBUG,
        filename=f"{base_directory}snellius/run-algorithm.log",
    )

    try:
        logging.info("")
        logging.info("===============================================================")
        logging.info("Start the ARISE detection algorithm in a Singularity container.")

        execute_through_shell = False

        if execute_through_shell:
            command = (
                f"{apptainer_command_path} run "
                f"--bind {base_directory}snellius/shared:/usr/src/app "
                f"{base_directory}snellius/{algorithm_name}.sif"
            )
        else:
            command = [
                apptainer_command_path,
                "run",
                "--bind",
                f"{base_directory}snellius/shared:/usr/src/app",
                f"{base_directory}snellius/{algorithm_name}.sif",
            ]

        logging.debug(f"Command: {command}")

        if execute_through_shell:
            # The "shell=True" option is used because otherwise the --bind option does
            # not seem to work (Error for command "run": unknown flag: --bind).
            algorithm_process = subprocess.Popen(command, shell=True)

            # The "preexec_fn=os.setsid" option can be used to create a process group
            # with all sub processes, which makes termination easier.
            # algorithm_process = subprocess.Popen(
            #     command, shell=True, preexec_fn=os.setsid
            # )
        else:
            algorithm_process = subprocess.Popen(command)

        logging.info(f"Wait 10 seconds for the {algorithm_name} algorithm to start.")
        time.sleep(10)

        logging.debug(
            f"{algorithm_name} algorithm process ID: " f"{algorithm_process.pid}."
        )

        # Process the media item files in the input directory and write the results as
        # JSON files to the output directory.
        input_directory = f"{base_directory}snellius/input/"
        output_directory = f"{base_directory}snellius/output/"
        for input_file_name in fnmatch.filter(os.listdir(input_directory), "*.jpg"):
            logging.info(f"Processing file {input_file_name}.")
            files = {"image": open(f"{input_directory}{input_file_name}", "rb")}
            response = requests.post("http://0.0.0.0:5000/v1/analyse", files=files)
            logging.info("------")
            logging.info(
                f"Response from the {algorithm_name} algorithm: " f"{response.json()}."
            )

            output_file_name = (
                f"{output_directory}" f"{os.path.splitext(input_file_name)[0]}.json"
            )
            with open(output_file_name, "w", encoding="utf-8") as output_file:
                json.dump(response.json(), output_file, ensure_ascii=False, indent=4)
            logging.info("======")

        logging.info(f"Stop the {algorithm_name} algorithm.")
        # algorithm_process.terminate()
        # algorithm_process.kill()

        # Terminating or killing the algorithm_process leaves several processes alive,
        # so we kill the child processes here explicitly. (The amount of surviving
        # processes seems to also be influenced by execute_through_shell.)

        # Local laptop:
        #
        # (venv) freek@naturalis-lt7268:~/projects/algorithm-template$ ps ux |
        #     grep -i apptainer
        #
        # freek     531310  4.0  0.0   5716  3476 pts/5    S    16:45   0:01
        #     /usr/bin/squashfuse -f -o uid=1000,gid=1000,offset=49152 /proc/self/fd/3
        #     /var/lib/apptainer/mnt/session/rootfs

        current_process = psutil.Process()
        children = current_process.children(recursive=True)
        children.reverse()
        for child in children:
            logging.debug(
                f"Terminating the child process {child.name()} "
                f"with process ID {child.pid}."
            )
            os.kill(child.pid, signal.SIGTERM)

        # Send the termination signal to all processes in the process group.
        # os.killpg(os.getpgid(algorithm_process.pid), signal.SIGTERM)

    except OSError:
        logging.exception("OS error occurred.")
    except ValueError:
        logging.exception(
            f"Starting the {algorithm_name} algorithm was called with invalid"
            f"arguments."
        )
    except CalledProcessError:
        logging.exception(
            f"The {algorithm_name} algorithm returned a non-zero return code."
        )


if __name__ == "__main__":
    main()
